﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.
$(document).ready(function () {

    $("#cash").on("click", function () {
        $('#showcredit').addClass('hidden');
        $("#showcash").removeClass('hidden');
        $("#credit").removeClass('select');
        $('#cash').addClass('select');
    });
    $("#credit").on("click", function () {
        $('#showcash').addClass('hidden');
        $("#showcredit").removeClass('hidden');
        $("#cash").removeClass('select');
        $('#credit').addClass('select');
    });
});