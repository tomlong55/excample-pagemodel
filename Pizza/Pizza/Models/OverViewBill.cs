﻿using Pizza.Enititys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizza.Models
{
    public class OverViewBill : AllBill
    {
        public string customName { get; set; }
        public int countItem { get; set; }
        public float? totalPrice { get; set; }
    }
}
