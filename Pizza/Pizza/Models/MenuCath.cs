﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizza.Models
{
    public class MenuCath
    {
        public int Menuid { get; set; }
        public string Menuname { get; set; }
        public int Typeid { get; set; }
        public float Price { get; set; }
        public int QtyOrder { get; set; }
        //public int Qty { get; set; }
        public bool? Ishide { get; set; }
        public DateTime? Datecreate { get; set; }
        public string Pic { get; set; }
        public string SizeName { get; set; }
        public int? Sizeid { get; set; }
        public int? SizePrice { get; set; }
        public int? ToppingidPrice1 { get; set; } = 0;
        public int? ToppingidId1 { get; set; }
        public string ToppingidName1 { get; set; }
        public int? ToppingidQty1 { get; set; } = 0;
        public int? ToppingidPrice2 { get; set; } = 0;
        public int? ToppingidId2 { get; set; }
        public int? ToppingidQty2 { get; set; } = 0;
        public string ToppingidName2 { get; set; }
        public int? ToppingidPrice3 { get; set; } = 0;
        public int? ToppingidQty3 { get; set; } = 0;
        public int? ToppingidId3 { get; set; }
        public string ToppingidName3 { get; set; }
        public int? ToppingidPrice4 { get; set; } = 0;
        public int? ToppingidQty4 { get; set; } = 0;
        public int? ToppingidId4 { get; set; }
        public string ToppingidName4 { get; set; }
    }
}
