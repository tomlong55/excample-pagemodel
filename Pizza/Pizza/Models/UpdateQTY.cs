﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizza.Models
{
    public class UpdateQTY
    {
        public int Menuid { get; set; }
        public int Qty { get; set; }
    
    }
}
