﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pizza.Models
{
    public class bill
    {
        [Key]
        public string menu { get; set; }
        public int? qty { get; set; }
        public float? price { get; set; }
        public string sizename { get; set; }
        public int? sizeprice { get; set; }
        public int? Toppingid1 { get; set; }
        public int? Toppingid2 { get; set; }
        public int? Toppingid3 { get; set; }
        public int? Toppingid4 { get; set; }
        public int? Toppingprice1 { get; set; }
        public int? Toppingprice2 { get; set; }
        public int? Toppingprice3 { get; set; }
        public int? Toppingprice4 { get; set; }
        public string Toppingname1 { get; set; }
        public string Toppingname2 { get; set; }
        public string Toppingname3 { get; set; }
        public string Toppingname4 { get; set; }

    }
}
