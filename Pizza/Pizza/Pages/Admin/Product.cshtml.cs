﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Pizza.Enititys;

namespace Pizza
{
    public class ProductModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();
        private IHostingEnvironment _environment;
        public ProductModel(IHostingEnvironment environment)
        {
            _environment = environment;
        }
        public ActionResult OnGet()
        {
            menus = (from t1 in db.Menu
                     select t1).ToList();

            sizes = (from t1 in db.Size
                     select t1).ToList();

            toppings = (from t1 in db.Topping
                        select t1).ToList();

            typeList = db.Type.ToList();
            selectListItems = typeList.Select(c => new SelectListItem { Value = c.Typeid.ToString(), Text = c.Typename }).ToList();

            return Page();
        }
        public ActionResult OnPostUpdateMenu(List<Menu> menus)
        {
            foreach (var item in menus)
            {
                var menusdb = (from t1 in db.Menu
                               where t1.Menuid == item.Menuid
                               select t1).SingleOrDefault();

                menusdb.Menuname = item.Menuname;
                menusdb.Price = item.Price;
                menusdb.Qty = item.Qty;
                menusdb.Ishide = item.Ishide;
            }

            db.SaveChanges();

            return OnGet();
        }
        public ActionResult OnPostUpdateSizes(List<Size> sizes)
        {
            foreach (var item in sizes)
            {
                var sizesdb = (from t1 in db.Size
                               where t1.Sizeid == item.Sizeid
                               select t1).SingleOrDefault();

                sizesdb.Sizename = item.Sizename;
                sizesdb.Sizeprice = item.Sizeprice;
            }

            db.SaveChanges();

            return OnGet();
        }
        public ActionResult OnPostUpdatetoppings(List<Topping> toppings)
        {
            foreach (var item in toppings)
            {
                var toppingsdb = (from t1 in db.Topping
                                  where t1.Toppingid == item.Toppingid
                                  select t1).SingleOrDefault();

                toppingsdb.Toppingname = item.Toppingname;
                toppingsdb.Toppingprice = item.Toppingprice;
            }

            db.SaveChanges();

            return OnGet();
        }
        public ActionResult OnPostCreateMenu()
        {

            var path = Path.Combine(_environment.WebRootPath, "pic", Upload.FileName);
            Menu newMenu = new Menu();
            newMenu.Typeid = typeId;
            newMenu.Menuname = name;
            newMenu.Price = price;
            newMenu.Datecreate = DateTime.Now;
            newMenu.Description = description;
            newMenu.Pic = Upload.FileName;
  
            FileStream stream = new FileStream(path, FileMode.Create);
            Upload.CopyToAsync(stream);

            db.Menu.Add(newMenu);
            db.SaveChanges();
            return OnGet();

        }
        public ActionResult OnPostCreateSize()
        {

            Size size = new Size();
            size.Sizename = name;
            size.Sizeprice = price;

            db.Size.Add(size);
            db.SaveChanges();
            return OnGet();
        }
        public ActionResult OnPostCreateTopping()
        {

            Topping topping = new Topping();
            topping.Toppingname = name;
            topping.Toppingprice = price;

            db.Topping.Add(topping);
            db.SaveChanges();
            return OnGet();
        }

        public List<Menu> menus { get; set; } = new List<Menu>();
        public List<Size> sizes { get; set; } = new List<Size>();
        public List<Topping> toppings { get; set; } = new List<Topping>();

        [BindProperty]
        public List<SelectListItem> selectListItems { get; set; } = new List<SelectListItem>();
        [BindProperty]
        public List<Enititys.Type> typeList { get; set; } = new List<Enititys.Type>();
        [BindProperty]
        [Required(ErrorMessage = "Please select category.")]
        public int typeId { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter product name.")]
        public string name { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter price.")]
        public int price { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter product's description.")]
        public string description { get; set; }
        //[Required(ErrorMessage = "Please select product's picture.")]
        [BindProperty]
        public IFormFile Upload { get; set; }
    }
}