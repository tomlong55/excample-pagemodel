﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;
using Pizza.Models;

namespace Pizza
{
    public class BillDetailModel : PageModel
    {
        private PizzaprojectContext db = new PizzaprojectContext();
        public void OnGet(int id)
        {
            var head = db.AllBill.Where(c => c.Id == id).Single();
            var temp = db.Bill.Where(c => c.Allbillid == id).ToList();
            foreach (var a in temp)
            {
                var temp2 = new bill();
                temp2.menu = db.Menu.Where(c => c.Menuid == a.Menuid).Single().Menuname;
                temp2.qty = a.Qty;
                temp2.price = a.Price;
                detailList.Add(temp2);
            }

            var user = db.User.Where(d => d.Userid == head.Userid).Single();
            var subBill = db.Bill.Where(d => d.Allbillid == head.Id).ToList();
            overViewBill = new OverViewBill();
            overViewBill.Id = head.Id;
            overViewBill.Userid = head.Userid;
            overViewBill.Isactive = head.Isactive;
            overViewBill.Datecreate = head.Datecreate;
            overViewBill.customName = user.Username;
            overViewBill.countItem = subBill.Sum(c => c.Qty);
            overViewBill.totalPrice = subBill.Sum(c => c.Qty * c.Price);
        }
        [BindProperty]
        public OverViewBill overViewBill { get; set; } = new OverViewBill();
        [BindProperty]
        public List<bill> detailList { get; set; } = new List<bill>();

    }
}