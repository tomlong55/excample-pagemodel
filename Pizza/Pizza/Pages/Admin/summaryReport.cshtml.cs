﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;
using Pizza.Models;

namespace Pizza
{
    public class summaryReportModel : PageModel
    {
        private PizzaprojectContext db = new PizzaprojectContext();
        public void OnGet()
        {
            paidBill = getOverViewBill(true);
            nonPaidBill = getOverViewBill(false);
            summarubill();
        }
        public List<OverViewBill> getOverViewBill(bool isActive)
        {
            var paidAllBill = db.AllBill.Where(c => c.Isactive == isActive).ToList();
            //return paidAllBill.Select(c => new OverViewBill()
            //{
            //    Id = c.Id,
            //    Userid = c.Userid,
            //    Isactive = c.Isactive,
            //    Datecreate = c.Datecreate,
            //    customName = db.User.Where(d => d.Userid ==c.Userid).Single().Username,
            //    countItem = db.Bill.Where(d => d.Allbillid == c.Id).Sum(c => c.Qty),
            //    totalPrice = db.Bill.Where(d => d.Allbillid == c.Id).Sum(c => c.Price)
            //}).ToList();
            List<OverViewBill> output = new List<OverViewBill>();
            foreach (var bill in paidAllBill)
            {
                var user = db.User.Where(d => d.Userid == bill.Userid).Single();
                var subBill = db.Bill.Where(d => d.Allbillid == bill.Id).ToList();
                var overViewBill = new OverViewBill();
                overViewBill.Id = bill.Id;
                overViewBill.Userid = bill.Userid;
                overViewBill.Isactive = bill.Isactive;
                overViewBill.Datecreate = bill.Datecreate;
                overViewBill.customName = user.Username;
                overViewBill.countItem = subBill.Sum(c => c.Qty);
                overViewBill.totalPrice = subBill.Sum(c => (c.Qty * (c.Price+c.Sizeprice+c.Toppingprice1 + c.Toppingprice2 + c.Toppingprice3 + c.Toppingprice4)));
                output.Add(overViewBill);
            }
            return output;
        }
        public void summarubill()
        {

            var sum = 0;
            var count = 0;

            var tempactive = new List<string>();
            var q = from a in db.AllBill
                    where (a.Isactive == true)
                    select new { a.Id };

            foreach (var item in q)
            {
                tempactive.Add(item.Id.ToString());

            }
            foreach (var item in tempactive)
            {

                count++;
                var q2 = from a in db.Bill
                         where (a.Allbillid == Int32.Parse(item))
                         select a;

                foreach (var item2 in q2)
                {
                    sum = sum + (Int32.Parse(item2.Qty.ToString()) * (Int32.Parse(item2.Price.ToString()) +
                   Int32.Parse(item2.Toppingprice1.ToString() ?? "0") + Int32.Parse(item2.Toppingprice2.ToString() ?? "0") + 
                   Int32.Parse(item2.Toppingprice3.ToString() ?? "0")
                   + Int32.Parse(item2.Toppingprice4.ToString() ?? "0") + Int32.Parse(item2.Sizeprice.ToString() ?? "0")));
                }

            }
            summarybill billactive = new summarybill();
            billactive.name = "Bill is active";
            billactive.count = count;
            billactive.total = sum;
            summarybills.Add(billactive);
            sum = 0;
            count = 0;
            var tempnotactive = new List<string>();
            var q3 = from a in db.AllBill
                     where (a.Isactive == false)
                     select new { a.Id };

            foreach (var item in q3)
            {
                tempnotactive.Add(item.Id.ToString());

            }
            foreach (var item in tempnotactive)
            {
                count++;
                var q4 = from a in db.Bill
                         where (a.Allbillid == Int32.Parse(item))
                         select a;
                foreach (var item2 in q4)
                {
                    sum = sum + (Int32.Parse(item2.Qty.ToString()) * (Int32.Parse(item2.Price.ToString()) +
                   Int32.Parse(item2.Toppingprice1.ToString()) + Int32.Parse(item2.Toppingprice2.ToString()) + Int32.Parse(item2.Toppingprice3.ToString())
                   + Int32.Parse(item2.Toppingprice4.ToString()) + Int32.Parse(item2.Sizeprice.ToString())));
                }
            }
            summarybill billnotactive = new summarybill();
            billnotactive.name = "Bill is not active";
            billnotactive.count = count;
            billnotactive.total = sum;
            summarybills.Add(billnotactive);
        }
        [BindProperty]
        public List<summarybill> summarybills { get; set; } = new List<summarybill>();
        [BindProperty]
        public List<OverViewBill> paidBill { get; set; } = new List<OverViewBill>();
        [BindProperty]
        public List<OverViewBill> nonPaidBill { get; set; } = new List<OverViewBill>();



    }
}