﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;

namespace Pizza
{
    public class choicePayModel : PageModel
    {
        private PizzaprojectContext db = new PizzaprojectContext();
        public ActionResult OnGet(int AllBill)
        {
            //รับค่า Argument จากหน้า Cath


            var q = from a in db.Bill
                    where (a.Allbillid == AllBill)
                    select new
                    {
                       a.Sizeprice,
                        a.Qty,
                        a.Price,            
                        a.Toppingprice1,
                        a.Toppingprice2,
                        a.Toppingprice3,
                        a.Toppingprice4
                    };
            var sum = 0;
            foreach (var item in q)
            {
               
                //คำนวณยอดรวมของบิลนั้นๆ
                sum = sum + (Int32.Parse(item.Qty.ToString()) * (Int32.Parse(item.Price.ToString()) +
                   Int32.Parse(item.Toppingprice1.ToString()) + Int32.Parse(item.Toppingprice2.ToString()) + Int32.Parse(item.Toppingprice3.ToString())
                   + Int32.Parse(item.Toppingprice4.ToString()) + Int32.Parse(item.Sizeprice.ToString())));
            }
            //เป็นการเก็บค่าใส่ตัวแปรเพื่อนำไปใช้ในไฟล์ ChoicePay.cshtml เพื่อนำไปใช้ต่อ
            ViewData["showAlert"] = false;
            ViewData["Bill"] = AllBill;
            ViewData["sum"] = sum;
            TempData["sum"] = sum;
            if (AllBill == 0)
            {  //หากไม่มีค่า Billส่งมาให้ย้อนกลับไปหน้า Menu
                return RedirectToPage("./Menu", "OnGet");
           
            }
            return Page();
        }
  
        public ActionResult OnPostCredit()
        {
            //หากเลือกชำระแบบบัตรเครดิต

            var ID = Int32.Parse(Request.Form["id"]);
            //เป็นการตรวจสอบการใส่ข้อมูลบัตรเครดิต
            if (Cardnumber == null)
            {
                //ไม่ใส่เลขบัตร
                ViewData["Bill"] = ID;
                return Page();
            }
            var number = Cardnumber.ToString();
            if (number.Length < 15)
            {
                //ใส่เลขบัตรผิด
                ViewData["showAlert"] = true;
                ViewData["Message"] = "Credit Card Incorrect";
                ViewData["Bill"] = ID;
                ViewData["sum"] = TempData["sum"];
                
                return Page();
            }else
            {
                ViewData["showAlert"] = false;
            }
            if (cvv == null || cvv.ToString().Length != 3)
            {
               //ใส่ cvv ผิด
                ViewData["showAlert"] = true;
                ViewData["Message"] = "CVV Incorrect";
                ViewData["Cardnumber"] = number;
                ViewData["Bill"] = ID;
                ViewData["sum"] = TempData["sum"];
                return Page();
            }
            else
            {
                ViewData["showAlert"] = false;
            }
            if (cardname == null)
            {
               //ไม่ใช่ชื่อบนบัตรเครดิต
                ViewData["Cardnumber"] = number;
                ViewData["cvv"] = cvv;
                ViewData["Bill"] = ID;
                ViewData["sum"] = TempData["sum"];
                return Page();
            }
            number = number.Remove(5, 9).Insert(5, "xxxx-xxxx");
            //หากข้อมูลถูกต้องทั้งหมดแล้ว จะส่งparameter ไปยังหน้า bill
            return RedirectToPage("./bill", "OnGet", new { numbercard = number, cash = -1, Bill = ID });
        }

        public ActionResult OnPostCash()
        {
            //หากเลือกเงินสด

            var Money = Int32.Parse(money.ToString());
            var ID = Int32.Parse(Request.Form["id"]);
            //ดึงข้อมูลของยอดสั่งทั้งหมดของบิลนั้นเพื่อนำมาคำนวณยอดสั่งซื้อทั้งหมด
            var q = from a in db.Bill
                    join b in db.AllBill on a.Allbillid equals b.Id
                    join c in db.User on b.Userid equals c.Userid
                    join d in db.Menu on a.Menuid equals d.Menuid
                    where (a.Allbillid == ID)
                    select new { c.Username, c.Address, c.Email, c.Phone, d.Menuname,
                        a.Sizeprice,
                        a.Qty,
                        a.Price,
                        a.Toppingprice1,
                        a.Toppingprice2,
                        a.Toppingprice3,
                        a.Toppingprice4,
                        b.Isactive };
            var sum = 0;

            foreach (var item in q)
            {
                //คำนวณยอดสั่งซื้อทั้งหมด
                sum = sum + (Int32.Parse(item.Qty.ToString()) * (Int32.Parse(item.Price.ToString()) +
                Int32.Parse(item.Toppingprice1.ToString()) + Int32.Parse(item.Toppingprice2.ToString()) + Int32.Parse(item.Toppingprice3.ToString())
                + Int32.Parse(item.Toppingprice4.ToString()) + Int32.Parse(item.Sizeprice.ToString())));

            }
            //เป็นการตรวจสอบจำนวนเงินที่ใส่
            if (Money < sum)
            {
                //หากจำนวนเงินรวมของบิลมากกว่าเงินที่เรากรอก
                ViewData["showAlert"] = true;
                ViewData["Message"] ="Cash Incorrect";
                ViewData["Bill"] = ID;
                ViewData["sum"] = sum;
                TempData["sum"] = sum;

                return Page();
            }
            else
            {
                ViewData["showAlert"] = false;
            
            }
            //หากข้อมูลถูกต้องทั้งหมดแล้ว จะส่งparameter ไปยังหน้า bill
            return RedirectToPage("./bill", "OnGet", new { numbercard = "-", cash = Money, Bill = ID });
        }

        [BindProperty]
        //[Required(ErrorMessage = " Please Enter Credit Card")]
        //[StringLength(20, MinimumLength = 19, ErrorMessage = "Credit Card Incorrect")]
        public string Cardnumber { get; set; }

        [BindProperty]
        public string Month { get; set; }
        public int id { get; set; }
        [BindProperty]
        //[Required(ErrorMessage = " Please Enter Cash")]
        public int money { get; set; }
        [BindProperty]
        //[Required(ErrorMessage = " Please Enter CVV")]
        //[StringLength(5, MinimumLength = 3, ErrorMessage = "CVV Incorrect")]
        public string cvv { get; set; }
        [BindProperty]
        //[Required(ErrorMessage = " Please Enter Name")]
        //[StringLength(18, MinimumLength = 3, ErrorMessage = "name Incorrect")]
        public string cardname { get; set; }
    }
}