﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Pizza.Enititys;
using Pizza.Models;
using Pizza.Pages.User;

namespace Pizza
{
    public class MenuDetailModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();

        public ActionResult OnGet(int? Menuid = null)
        {
            menus = (from t1 in db.Menu
                     where t1.Menuid == Menuid
                     select t1).ToList();

            HttpContext.Session.SetInt32("Menuid", Menuid.Value);
            
            sizes = (from t1 in db.Size
                     select t1).ToList();

            toppings = (from t1 in db.Topping
                        select t1).ToList();
            toppingsQty = toppings;

            selectListItems = sizes.Select(c => new SelectListItem { Value = c.Sizeid.ToString(), Text = c.Sizename }).ToList();

            return Page();
        }
        public ActionResult OnPostAddcart()
        {
            menu = HttpContext.Session.GetObjectFromJson<List<MenuCath>>("menu");

            if (menu == null)
            {
                int countqty = 0;
                foreach (var item in toppingsQty.ToList())
                {
                    if (item.Toppingqty != 0 )
                    {
                        countqty++;
                    }
                    else
                    {
                        toppingsQty.Remove(item);
                    }
                }
                if (countqty > 4)
                {

                    ModelState.AddModelError(string.Empty, "เลือกท็อปปิ้งเพิ่มเติมได้สูงสุด 4รายการเท่านั้น");

                    return OnGet(HttpContext.Session.GetInt32("Menuid"));
                }
                HttpContext.Session.SetObjectAsJson("menu", menus);

                menu = HttpContext.Session.GetObjectFromJson<List<MenuCath>>("menu");
                menu[0].QtyOrder = 1;
               
                    menu[0].Sizeid = SizeId;
                    menu[0].SizeName = (from t1 in db.Size
                                        where t1.Sizeid == SizeId
                                        select t1.Sizename).Single();

                    menu[0].SizePrice = (from t1 in db.Size
                                         where t1.Sizeid == SizeId
 select t1.Sizeprice).Single();
                   if (menus[0].Typeid == 1)
                {                      
                    if (toppingsQty.Count >= 1)
                    {
                        menu[0].ToppingidId1 = toppingsQty[0].Toppingid;
                        menu[0].ToppingidName1 = toppingsQty[0].Toppingname;
                        menu[0].ToppingidPrice1 = toppingsQty[0].Toppingprice;
                        menu[0].ToppingidQty1 = toppingsQty[0].Toppingqty;
                        if (toppingsQty.Count >= 2)
                        {
                            menu[0].ToppingidId2 = toppingsQty[1].Toppingid;
                            menu[0].ToppingidName2 = toppingsQty[1].Toppingname;
                            menu[0].ToppingidPrice2 = toppingsQty[1].Toppingprice;
                            menu[0].ToppingidQty2 = toppingsQty[1].Toppingqty;
                            if (toppingsQty.Count >= 3)
                            {
                                menu[0].ToppingidId3 = toppingsQty[2].Toppingid;
                                menu[0].ToppingidName3 = toppingsQty[2].Toppingname;
                                menu[0].ToppingidPrice3 = toppingsQty[2].Toppingprice;
                                menu[0].ToppingidQty3 = toppingsQty[2].Toppingqty;
                                if (toppingsQty.Count == 4)
                                {
                                    menu[0].ToppingidId4 = toppingsQty[3].Toppingid;
                                    menu[0].ToppingidName4 = toppingsQty[3].Toppingname;
                                    menu[0].ToppingidPrice4 = toppingsQty[3].Toppingprice;
                                    menu[0].ToppingidQty4 = toppingsQty[3].Toppingqty;
                                }
                            }
                        }
                    }
                }



                HttpContext.Session.SetObjectAsJson("menu", menu);
            }
            else
            {
                int countqty = 0;
                foreach (var item in toppingsQty.ToList())
                {
                    if (item.Toppingqty != 0 )
                    {
                        countqty++;
                    }
                    else
                    {
                        toppingsQty.Remove(item);
                    }
                }
                if (countqty > 4)
                {
                    ModelState.AddModelError(string.Empty, "เลือกท็อปปิ้งเพิ่มเติมได้สูงสุด 4รายการเท่านั้น");

                    return OnGet(HttpContext.Session.GetInt32("Menuid"));
                }

                MenuCath menu1 = new MenuCath();
                menu1.Menuid = menus[0].Menuid;
                menu1.Menuname = menus[0].Menuname;
                menu1.Typeid = menus[0].Typeid;
                menu1.Price = menus[0].Price;

                menu1.QtyOrder = 1;
               
                    menu1.Sizeid = SizeId;
                    menu1.SizeName = (from t1 in db.Size
                                      where t1.Sizeid == SizeId
                                      select t1.Sizename).Single();

                    menu1.SizePrice = (from t1 in db.Size
                                       where t1.Sizeid == SizeId
                                       select t1.Sizeprice).Single();
 if (menus[0].Typeid == 1)
                {
                    if (toppingsQty.Count >= 1)
                    {
                        menu1.ToppingidId1 = toppingsQty[0].Toppingid;
                        menu1.ToppingidName1 = toppingsQty[0].Toppingname;
                        menu1.ToppingidPrice1 = toppingsQty[0].Toppingprice;
                        menu1.ToppingidQty1 = toppingsQty[0].Toppingqty;
                        if (toppingsQty.Count >= 2)
                        {
                            menu1.ToppingidId2 = toppingsQty[1].Toppingid;
                            menu1.ToppingidName2 = toppingsQty[1].Toppingname;
                            menu1.ToppingidPrice2 = toppingsQty[1].Toppingprice;
                            menu1.ToppingidQty2 = toppingsQty[1].Toppingqty;
                            if (toppingsQty.Count >= 3)
                            {
                                menu1.ToppingidId3 = toppingsQty[2].Toppingid;
                                menu1.ToppingidName3 = toppingsQty[2].Toppingname;
                                menu1.ToppingidPrice3 = toppingsQty[2].Toppingprice;
                                menu1.ToppingidQty3 = toppingsQty[2].Toppingqty;
                                if (toppingsQty.Count == 4)
                                {
                                    menu1.ToppingidId4 = toppingsQty[3].Toppingid;
                                    menu1.ToppingidName4 = toppingsQty[3].Toppingname;
                                    menu1.ToppingidPrice4 = toppingsQty[3].Toppingprice;
                                    menu1.ToppingidQty4 = toppingsQty[3].Toppingqty;
                                }
                            }
                        }
                    }

                }

                menu.Add(menu1);

                HttpContext.Session.SetObjectAsJson("menu", menu);
            }

             ModelState.AddModelError(string.Empty, "สินค้าถูกเพิ่มเข้าไปแล้ว");

            return OnGet(HttpContext.Session.GetInt32("Menuid"));
        }

        [BindProperty]
        public List<SelectListItem> selectListItems { get; set; } = new List<SelectListItem>();

        [BindProperty]
        public List<Menu> menus { get; set; } = new List<Menu>();

        public List<MenuCath> menu = new List<MenuCath>();
        public List<Size> sizes { get; set; } = new List<Size>();

        [BindProperty]
        public List<Topping> toppings { get; set; } = new List<Topping>();

        [BindProperty]
        public List<Topping> toppingsQty { get; set; } = new List<Topping>();

        [BindProperty]
        public int? SizeId { get; set; }

    }
}