﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;
using Pizza.Models;

namespace Pizza
{
    public class billModel : PageModel
    {
        private PizzaprojectContext db = new PizzaprojectContext();
        public ActionResult OnGet(string numbercard, int cash, int Bill)
        {
            //รับค่า Argument จากหน้า choicePay
            if (Bill == 0)
            {
                //หากไม่มีค่า Billส่งมาให้ย้อนกลับไปหน้า Menu
                return RedirectToPage("./Menu", "OnGet");
            }
            //เป็นการเก็บค่าใส่ตัวแปรเพื่อนำไปใช้ในไฟล์ bill.cshtml เพื่อแสดงข้อมูลรายละเอียดของบิล
            TempData["numbercard"] = numbercard;
            TempData["cash"] = cash;
            TempData["Bill"] = Bill;
            ViewData["status"] = "";
            //เรียก function detail
            detail(numbercard, cash, Bill);
            return Page();

        }
   
        public void detail(string numbercard, int cash, int Bill)
        {
            //เป็น functionที่ใช้ในการแสดงรายละเอียดเมนูของบิลนั้นๆ

            ViewData["Bill"] = Bill;
            var q = from a in db.Bill
                    join b in db.AllBill on a.Allbillid equals b.Id
                    join c in db.User on b.Userid equals c.Userid
                    join d in db.Menu on a.Menuid equals d.Menuid
                    join e in db.Size on a.Sizeid equals e.Sizeid
                    //join t1 in db.Topping on a.Toppingid1 equals t1.Toppingid
                    //join t2 in db.Topping on a.Toppingid2 equals t2.Toppingid
                    //join t3 in db.Topping on a.Toppingid3 equals t3.Toppingid
                    //join t4 in db.Topping on a.Toppingid4 equals t4.Toppingid
                    where (a.Allbillid == Bill)
                    select new { c.Username, c.Address, c.Email, c.Phone, d.Menuname, a.Qty, a.Price ,b.Isactive,e.Sizename,e.Sizeprice,
                        a.Toppingid1,a.Toppingid2,a.Toppingid3,a.Toppingid4,a.Toppingprice1 , a.Toppingprice2 , a.Toppingprice3,a.Toppingprice4  };
                  
            var sum = 0;

            // ดึงข้อมูลที่เกี่ยวข้องกับบิลนั้นทั้งหมด
            foreach (var item in q)
            {
                if (item.Isactive == true)
                {
                    //หากบิลนั้น ถูกทำการ Checkbill แล้ว
                    ViewData["status"] = "update";
                }
                //คำนวณยอดรวมของบิลนั้นๆ
                sum = sum + (Int32.Parse(item.Qty.ToString()) * (Int32.Parse(item.Price.ToString()) +
                   Int32.Parse(item.Toppingprice1.ToString())+ Int32.Parse(item.Toppingprice2.ToString())+ Int32.Parse(item.Toppingprice3.ToString())
                   + Int32.Parse(item.Toppingprice4.ToString())+ Int32.Parse(item.Sizeprice.ToString())));
                //เป็นการเก็บค่าใส่ตัวแปรเพื่อนำไปใช้ในไฟล์ bill.cshtml เพื่อแสดงข้อมูลรายละเอียดของบิล
                ViewData["Sum"] = sum;
                ViewData["Address"] = item.Address;
                ViewData["Username"] = item.Username;
                ViewData["Email"] = item.Email;
                ViewData["Phone"] = item.Phone;

                bill itemlist = new bill();
                itemlist.menu = item.Menuname;
                itemlist.qty = item.Qty;
                itemlist.price = item.Price;
                itemlist.sizename = item.Sizename;
                itemlist.sizeprice = item.Sizeprice;
                itemlist.Toppingid1 = item.Toppingid1;
                itemlist.Toppingid2 = item.Toppingid2;
                itemlist.Toppingid3 = item.Toppingid3;
                itemlist.Toppingid4 = item.Toppingid4;
                itemlist.Toppingprice1 = item.Toppingprice1;
                itemlist.Toppingprice2 = item.Toppingprice2;
                itemlist.Toppingprice3 = item.Toppingprice3;
                itemlist.Toppingprice4 = item.Toppingprice4;

                list.Add(itemlist);
                //list.Add(new bill
                //{
                //    menu = item.Menuname,
                //    qty = item.Qty,
                //    price = item.Price

                //});
            }

            for (int i = 0; i < list.Count; i++)
            {

                if (list[i].Toppingid1 != null)
                {
                    var getname = from a in db.Topping
                                  where (a.Toppingid == list[i].Toppingid1)
                                  select new { a.Toppingname };

                    foreach (var item in getname)
                    {
                        list[i].Toppingname1 = item.Toppingname;
                    }

                }
                if (list[i].Toppingid2 != null)
                {
                    var getname = from a in db.Topping
                                  where (a.Toppingid == list[i].Toppingid2)
                                  select new { a.Toppingname };

                    foreach (var item in getname)
                    {
                        list[i].Toppingname2 = item.Toppingname;
                    }

                }
                if (list[i].Toppingid3 != null)
                {
                    var getname = from a in db.Topping
                                  where (a.Toppingid == list[i].Toppingid3)
                                  select new { a.Toppingname };

                    foreach (var item in getname)
                    {
                        list[i].Toppingname3 = item.Toppingname;
                    }

                }
                if (list[i].Toppingid4 != null)
                {
                    var getname = from a in db.Topping
                                  where (a.Toppingid == list[i].Toppingid4)
                                  select new { a.Toppingname };

                    foreach (var item in getname)
                    {
                        list[i].Toppingname4 = item.Toppingname;
                    }

                }



                //เป็นการแยกเงื่อนไขระหว่างชำระด้วยบัตรเครดิตและเงินสด
                if (cash == -1)
                {
                    //กรณีชำระด้วยบัตรเครดิต
                    ViewData["Numbercard"] = numbercard;
                    ViewData["Change"] = "-";
                }
                else
                {
                    //กรณีชำระด้วยเงินสด
                    ViewData["Numbercard"] = "-";
                    ViewData["Change"] = cash - sum;
                }
            }
        }
        public void OnPostUpdate()
        {
           // เมื่อทำการคลิก Confirm จะเข้า functionนี้ เป็นการอัพเดทว่าบิลนี้ได้ทำการยืนยันการชำระแล้ว
            var ID = Int32.Parse(Request.Form["id"]);
            var update = from c in db.AllBill
                         where c.Id == ID
                         select c;


            foreach (AllBill c in update)
            {
                //อัพเดทสถานะของบิลเป็นชำระแล้ว
                c.Isactive = true;
            }

            //เป็นการตัดยอดของใน stock ลง
            var q = from a in db.Bill
                    where (a.Allbillid == ID)
                    select new {a.Menuid,a.Qty };
            //ดึงข้อมูลของบิลนั้น
            foreach (var item in q)
            {
                //ใส่ข้อมูลของบบิลนั้นใส่ตัวตัวแปร temp ที่เป็น list
                UpdateQTY menulist = new UpdateQTY();
                menulist.Menuid = item.Menuid;
                menulist.Qty = item.Qty;
                temp.Add(menulist);

            }
            for (int i = 0; i < temp.Count; i++)
            {
                //ทำการไล่อัพเดทของ stock ตามตัวแปร tempที่ได้สร้างไว้
                var updateQTY = from c in db.Menu
                                where c.Menuid == temp[i].Menuid
                                select c;
                foreach (Menu c in updateQTY)
                {
                    c.Qty = c.Qty - temp[i].Qty;
                }
            }
       
           
            db.SaveChanges();
            ViewData["status"] = "update";
            HttpContext.Session.Remove("menu");

            //เรียก function detail เพื่อให้แสดงข้อมูลหลังจากการกด Confirm
            detail(TempData["numbercard"].ToString(), Int32.Parse(TempData["cash"].ToString()), Int32.Parse(TempData["Bill"].ToString()));


        }
        [BindProperty]
        public List<bill> list { get; set; } = new List<bill>();
        public List<UpdateQTY> temp { get; set; } = new List<UpdateQTY>();

    }
}