﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;
using Pizza.Models;
using Pizza.Pages.User;

namespace Pizza
{
    public class CathModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();

        public ActionResult OnGet()
        {
            menus = HttpContext.Session.GetObjectFromJson<List<MenuCath>>("menu");
            return Page();
        }
        public ActionResult OnPostCon()
        {
            
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
            int allbillid = 0;
            AllBill allBill = new AllBill();

            allBill.Userid = userid;
            allBill.Datecreate = DateTime.Now;

            db.AllBill.Add(allBill);

            db.SaveChanges();

            allbillid = allBill.Id;

            foreach (var item in menus)
            {
                Bill bill = new Bill();

                bill.Allbillid = allbillid;
                bill.Menuid = item.Menuid;
                bill.Qty = item.QtyOrder;
                bill.Price = item.Price;
                bill.Datecreate = DateTime.Now;
                bill.Sizeid = item.Sizeid ?? 0;
                bill.Sizeprice = item.SizePrice ?? 0;
                bill.Toppingid1 = item.ToppingidId1 ?? 0; 
                bill.Toppingprice1 = item.ToppingidPrice1 ?? 0 * item.ToppingidQty1 ?? 0;
                bill.Toppingid2 = item.ToppingidId2 ?? 0;
                bill.Toppingprice2 = item.ToppingidPrice2 ?? 0 * item.ToppingidQty2 ?? 0;
                bill.Toppingid3 = item.ToppingidId3 ?? 0;
                bill.Toppingprice3 = item.ToppingidPrice3 ?? 0 * item.ToppingidQty3 ?? 0;
                bill.Toppingid4 = item.ToppingidId4 ?? 0;
                bill.Toppingprice4 = item.ToppingidPrice4 ?? 0 * item.ToppingidQty4 ?? 0;

                db.Bill.Add(bill);
            }
            db.SaveChanges();
            var f=ViewData["sum"];
            var w=TempData["sum"];

            return RedirectToPage("./choicePay", "OnGet", new { AllBill = allbillid });
        }
        public ActionResult OnPostCutoutProduct(int i)
        {
            menus = HttpContext.Session.GetObjectFromJson<List<MenuCath>>("menu");
            menus.RemoveAt(i);

            HttpContext.Session.Remove("menu");
            HttpContext.Session.SetObjectAsJson("menu", menus);

            ModelState.Clear();
       
          
            return Page();
        }

        [BindProperty]
        public List<MenuCath> menus { get; set; } = new List<MenuCath>();

    }
}