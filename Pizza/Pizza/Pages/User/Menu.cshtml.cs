﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;

namespace Pizza
{
    public class MenuModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();

        public ActionResult OnGet()
        {
            menus = (from t1 in db.Menu
                     where !t1.Ishide
                     select t1).ToList();

            return Page();
        }
        //public ActionResult OnPostAddCart()
        //{
        //    var a = menus;

        //    return OnGet();
        //}

        [BindProperty]
        public List<Menu> menus { get; set; } = new List<Menu>();
    }
}