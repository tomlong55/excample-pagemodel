﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Pizza.Enititys;

namespace Pizza.Pages
{
    public class IndexModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public IActionResult OnGet()
        {
            try
            {
                // Verification.  
                if (this.User.Identity.IsAuthenticated)
                {
                    if (User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault() == "True")
                    {
                        return RedirectToPage("Admin/summaryReport");
                    }
                    else
                    {
                    return RedirectToPage("User/Menu");

                    }
                    // Go to Home Page.  
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return Page();
        }
        public IActionResult OnPostGoToRegister()
        {
            return RedirectToPage("/Register");
        }
        public ActionResult OnPostLogIn()
        {
            if (username == null || password == null)
            {
                return Page();
            }
            Enititys.User user = new Enititys.User();
            user = (from t1 in db.User
                    where t1.Username == username && t1.Password == password
                    select t1).SingleOrDefault();

            if (user != null)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, user.Username));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Userid.ToString()));
                claims.Add(new Claim(ClaimTypes.Role, user.Isadmin.ToString()));
                
                //claims.Add(new Claim(ClaimTypes.Role, user.RoleId.ToString()));

                var claimIdenties = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimPrincipal = new ClaimsPrincipal(claimIdenties);
                var authenticationManager = Request.HttpContext;

                authenticationManager.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimPrincipal, new AuthenticationProperties() { IsPersistent = false });

                //return RedirectToPage("/Stock/User/FormList");

                // return RedirectToPage("/Stock/Admin/Admin");
                //if (true) //user.RoleId == 1
               
                //{
                if (user.Isadmin)
                {
                    return RedirectToPage("Admin/summaryReport");

                }
                else
                {
                    return RedirectToPage("User/Menu");

                }

                //}
                //else
                //{
                //    return RedirectToPage("/Privacy");
                //}
                //return Page();
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid username or password.");
                return Page();
            }
        }
        public ActionResult OnPostLogOff()
        {
            try
            {
                // Setting.  
                var authenticationManager = Request.HttpContext;
                HttpContext.Session.Remove("menu");
                // Sign Out.  
                authenticationManager.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return this.RedirectToPage("/Index");
        }
        #region variable
        [BindProperty]
        [Required(ErrorMessage = "Please enter your username.")]
        public string username { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter your password.")]
        public string password { get; set; }
        #endregion
    }
}
