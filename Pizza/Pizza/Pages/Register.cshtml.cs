﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Pizza.Enititys;

namespace Pizza.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly PizzaprojectContext db = new PizzaprojectContext();

        public void OnGet()
        {

        }
        public IActionResult OnPostGoToLogin()
        {
            return RedirectToPage("/Index");
        }
        public ActionResult OnPostRegister()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (username == null || password == null)
            {
                return Page();
            }
            Enititys.User user = new Enititys.User();
            user = (from t1 in db.User
                    where t1.Username == username
                    select t1).SingleOrDefault();

            if (user != null)
            {
                ModelState.AddModelError(string.Empty, "Username is already existed.");
                return Page();
            }
            else
            {
                Enititys.User newUser = new Enititys.User();
                newUser.Username = username;
                newUser.Password = password;
                newUser.Phone = phone;
                newUser.Email = email;
                newUser.Address = address;
                db.User.Add(newUser);
                db.SaveChanges();
                //ModelState.AddModelError(string.Empty, "This user has been created.");
                return RedirectToPage("/Index");
            }
        }
        #region variable
        [BindProperty]
        [Required(ErrorMessage = "Please enter your username.")]
        public string username { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter your password.")]
        public string password { get; set; }
        [BindProperty]
        //[Required(ErrorMessage = "Please enter your password.")]
        public string phone { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter your email.")]
        public string email { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "Please enter your address.")]
        public string address { get; set; }
        #endregion
    }
}