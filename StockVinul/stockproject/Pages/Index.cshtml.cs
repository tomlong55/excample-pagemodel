﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using stockproject.Enititys;
using stockproject.ExternalModels;

namespace stockproject.Pages
{
    public class IndexModel : PageModel
    {
        private readonly StockprojectContext db = new StockprojectContext();
        public IActionResult OnGet() //IActionResult
        {
            try
            {
                // Verification.  
                if (this.User.Identity.IsAuthenticated)
                {
                    // Go to Home Page.  
                    if (User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault() == "2")
                    {
                        return RedirectToPage("/Stock/User/User");
                    }
                    else
                    {
                        return RedirectToPage("/Stock/Admin/Admin");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return Page();
        }
        public ActionResult OnPostLogIn()
        {
            if (username == null || password == null)
            {
                return Page();
            }
            Enititys.User user = new Enititys.User();
            user = (from t1 in db.User
                    where t1.Username == username && t1.Password == password
                    select t1).SingleOrDefault();

            if (user != null)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, user.FullName));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Userid.ToString()));
                claims.Add(new Claim(ClaimTypes.Role, user.Roleid.ToString()));

                var claimIdenties = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimPrincipal = new ClaimsPrincipal(claimIdenties);
                var authenticationManager = Request.HttpContext;

                authenticationManager.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimPrincipal, new AuthenticationProperties() { IsPersistent = false });

                //return RedirectToPage("/Stock/User/User");

                // return RedirectToPage("/Stock/Admin/Admin");
                if (user.Roleid != 1)
                {
                    return RedirectToPage("/Stock/User/User");
                }
                else
                {
                    return RedirectToPage("/Stock/Admin/Admin");
                }
                //return Page();
            }
            else
            {
                ModelState.AddModelError(string.Empty, "ชื่อผู้ใช้งาน หรือรหัสผ่านของคุณไม่ถูกต้อง");
                return Page();
            }
        }
        public ActionResult OnPostLogOff()
        {
            try
            {
                // Setting.  
                var authenticationManager = Request.HttpContext;

                // Sign Out.  
                authenticationManager.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return this.RedirectToPage("/Index");
        }

        [BindProperty]
        [Required(ErrorMessage = "กรุณากรอกชื่อผู้ใช้งาน")]
        public string username { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "กรุณากรอกรหัสผ่าน")]
        public string password { get; set; }
    }
}

