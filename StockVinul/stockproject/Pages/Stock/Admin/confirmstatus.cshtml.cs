﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.ExternalModels;

namespace stockproject
{

    public class confirmstatusModel : PageModel
    {
        /*
    * หน้า confirmstatus มีไว้ให้ admin ดูรายละเอียดของบิลและ confrim หรือ reject ที่หน้านี้
   
    */
        private StockprojectContext db = new StockprojectContext();

        [BindProperty]
        public List<AdminConfirm> list { get; set; } = new List<AdminConfirm>();
        [BindProperty]
        public List<DetailForm> listtest { get; set; } = new List<DetailForm>();
        public List<ProductFormIn> temp { get; set; } = new List<ProductFormIn>();
        public List<ProductFormOut> temp2 { get; set; } = new List<ProductFormOut>();

        [BindProperty]
        public ResultForm formHeader { get; set; }
        [BindProperty]
        public List<DetailForm> formDetail { get; set; } = new List<DetailForm>();
        public string typeInText { get; set; } = "รายการนำสินค้าเข้า";
        public string typeOutText { get; set; } = "รายการจ่ายสินค้าออก";

        //ฟังชั่น OnGet รับค่ามาจากหน้า Admin ที่กดปุ่มVALUE
        public ActionResult OnGet(int formid, string formtype,string err = null)
        {
            list.Clear();
            listtest.Clear();
            temp.Clear();
            temp2.Clear();
            formDetail.Clear();
            if (formtype == "รายการนำสินค้าเข้า")
            {
                formtype = "FormIn";
            }
            else if (formtype == "รายการจ่ายสินค้าออก")
            {
                formtype = "FormOut";
            }

            if (err != null)
            {
                ModelState.AddModelError(string.Empty, "" + err + " ของใน Stock มีไม่พอ");
            }
            //คำสั่งรับค่าจากระบบที่ทำการ login ที่ระบบว่าคนนี้คือใคร ในที่นี้เอาค่าที่อยู่ในตัวแปร NameIdentifier ออกมาเป็นไว้ในตัวแปร userid และแปลงค่าเป็น int
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
            ViewData["confirm"] = "Yes";
            if (formtype == "FormIn")
            {
                formHeader = db.FormIn.Select(a => new ResultForm()
                {
                    Formid = a.FormInid,
                    IsIn = false,
                    Status = a.Status,
                    Createdate = a.Createdate,
                    Statusdate = a.Statusdate,
                    Userid = a.Userid

                }).Single(a => a.Formid == formid);
                ViewData["id"] = formid;
                ViewData["type"] = typeInText;
                var q2 = from a in db.ProductFormIn
                         join b in db.FormIn on a.FormInid equals b.FormInid
                         join c in db.User on b.Userid equals c.Userid
                         join d in db.Product on a.ProductId equals d.Productid
                         where (b.FormInid == formid)
                         select new { c.FullName, b.Createdate, b.Isstatus, d.Productname, a.Qty, a.Price, a.ProductId, d.Picpath };
                foreach (var item in q2)
                {
                    if (item.Isstatus == true)
                    {
                        ViewData["status"] = "True";
                    }
                    else
                    {
                        ViewData["status"] = "False";
                    }

                    ViewData["date"] = item.Createdate;
                    ViewData["name"] = item.FullName;

                    AdminConfirm confirm = new AdminConfirm();
                    confirm.product = item.Productname;
                    confirm.qty = item.Qty;
                    confirm.price = item.Price;
                    confirm.productid = item.ProductId;
                    confirm.Picpath = item.Picpath;
                    list.Add(confirm);


                    //list.Add(new AdminConfirm
                    //{
                    //    product = item.Productname,
                    //    qty = item.Qty,
                    //    // Createdate = item.Createdate,
                    //    price = item.Price,
                    //    productid= item.ProductId
                    //    //name= item.Username
                    //});
                }
            }

            else
            {

                formHeader = db.FormOut.Select(a => new ResultForm()
                {
                    Formid = a.FormOutid,
                    IsIn = false,
                    Status = a.Status,
                    Createdate = a.Createdate,
                    Statusdate = a.Statusdate,
                    Userid = a.Userid
                }).Single(a => a.Formid == formid);
                ViewData["id"] = formid;
                ViewData["type"] = typeOutText;
                var q2 = from a in db.ProductFormOut
                         join b in db.FormOut on a.FormOutid equals b.FormOutid
                         join c in db.User on b.Userid equals c.Userid
                         join d in db.Product on a.ProductId equals d.Productid
                         where (b.FormOutid == formid)
                         select new { c.FullName, b.Createdate, b.Isstatus, d.Productname, a.Qty, a.Price, a.ProductId, d.Picpath };

                foreach (var item in q2)
                {
                    if (item.Isstatus == true)
                    {
                        ViewData["status"] = "True";
                    }
                    else
                    {
                        ViewData["status"] = "False";
                    }
                    ViewData["date"] = item.Createdate;
                    ViewData["name"] = item.FullName;

                    AdminConfirm confirm = new AdminConfirm();
                    confirm.product = item.Productname;
                    confirm.qty = item.Qty;
                    confirm.price = item.Price;
                    confirm.productid = item.ProductId;
                    confirm.Picpath = item.Picpath;
                    list.Add(confirm);

                    //list.Add(new AdminConfirm
                    //{
                    //    product = item.Productname,
                    //    qty = item.Qty,
                    //    // Createdate = item.Createdate,
                    //    price = item.Price,
                    //    //name= item.Username
                    //    productid=item.ProductId
                    //});
                }


                for (int i = 0; i < list.Count; i++)
                {

                    var q3 = from a in db.Product
                             where (a.Productid == list[i].productid)
                             select a;

                    foreach (var item2 in q3)
                    {

                        if (list[i].qty > item2.Qty)
                        {
                            ModelState.AddModelError(string.Empty, "'" + item2.Productname + "' ของใน Stock มีไม่พอ ให่ยืนยันได้");
                            ViewData["confirm"] = "No";
                        }
                    }
                }

            }
            return Page();
        }

        public ActionResult OnPostCONFIRM(string id, string type)
        {
            if (type == typeInText)
            {
                var q = (from a in db.ProductFormIn
                        where (a.FormInid == Int32.Parse(id))
                        select new { a.ProductId, a.Qty }).ToList();

                foreach (var item in q)
                {
                    ProductFormIn formin = new ProductFormIn();
                    formin.ProductId = item.ProductId;
                    formin.Qty = item.Qty;
                    temp.Add(formin);
                    //temp.Add(new ProductFormIn
                    //{
                    //    ProductId = item.ProductId,
                    //    Qty = item.Qty,

                    //});

                }

                for (int i = 0; i < temp.Count; i++)
                {

                    var q2 = from a in db.Product
                             where (a.Productid == temp[i].ProductId)
                             select a;

                    foreach (Product item2 in q2)
                    {
                        item2.Qty = item2.Qty + temp[i].Qty;
                        item2.Modifydate = DateTime.Now;
                    }
                }

                var q3 = from c in db.FormIn
                         where c.FormInid == Int32.Parse(id)
                         select c;
                foreach (FormIn c in q3)
                {
                    c.Isstatus = true;
                    c.Status = "confirm";
                    c.Statusdate = DateTime.Now;
                }
                db.SaveChanges();

            }
            else if (type == typeOutText)
            {
                var q = from a in db.ProductFormOut
                        where (a.FormOutid == Int32.Parse(id))
                        select new { a.ProductId, a.Qty };

                foreach (var item in q)
                {
                    ProductFormOut formout = new ProductFormOut();
                    formout.ProductId = item.ProductId;
                    formout.Qty = item.Qty;
                    temp2.Add(formout);
                    //temp2.Add(new ProductFormOut
                    //{
                    //    ProductId = item.ProductId,
                    //    Qty = item.Qty,

                    //});

                }

                for (int i = 0; i < temp2.Count; i++)
                {

                    var q2 = from a in db.Product
                             where (a.Productid == temp2[i].ProductId)
                             select a;

                    foreach (Product item2 in q2)
                    {

                        item2.Qty = item2.Qty - temp2[i].Qty;
                        item2.Modifydate = DateTime.Now;

                    }
                }


                var q3 = from d in db.FormOut
                         where d.FormOutid == Int32.Parse(id)
                         select d;
                foreach (FormOut d in q3)
                {
                    d.Isstatus = true;
                    d.Status = "confirm";
                    d.Statusdate = DateTime.Now;
                }
                db.SaveChanges();
            }
            return RedirectToPage("./Admin", "OnGet");

        }
        public ActionResult OnPostREJECT(string id, string type)
        {

            if (type == typeInText)
            {

                var q = from c in db.FormIn
                        where c.FormInid == Int32.Parse(id)
                        select c;
                foreach (FormIn c in q)
                {
                    c.Isstatus = true;
                    c.Status = "reject";
                    c.Statusdate = DateTime.Now;
                }
                db.SaveChanges();

            }
            else if (type == typeOutText)
            {
                var q = from d in db.FormOut
                        where d.FormOutid == Int32.Parse(id)
                        select d;
                foreach (FormOut d in q)
                {
                    d.Isstatus = true;
                    d.Status = "reject";
                    d.Statusdate = DateTime.Now;
                }
                db.SaveChanges();
            }
            return RedirectToPage("./Admin", "OnGet");

        }
        public ActionResult OnPostDeleteForm(int formId, string typeform)
        {
            if (typeform == typeInText)
            {

                var query = from c in db.FormIn where (c.FormInid == formId) select c;
                foreach (FormIn c in query)
                {
                    db.FormIn.Remove(c);

                }

                var query2 = from c2 in db.ProductFormIn where (c2.FormInid == formId) select c2;
                foreach (ProductFormIn c2 in query2)
                {
                    db.ProductFormIn.Remove(c2);

                }
                db.SaveChanges();

            }
            else
            {
                var query = from c in db.FormOut where (c.FormOutid == formId) select c;
                foreach (FormOut c in query)
                {
                    db.FormOut.Remove(c);

                }

                var query2 = from c2 in db.ProductFormOut where (c2.FormOutid == formId) select c2;
                foreach (ProductFormOut c2 in query2)
                {
                    db.ProductFormOut.Remove(c2);

                }
                db.SaveChanges();
            }
            return RedirectToPage("/Stock/Admin/Admin");
        }
        public ActionResult OnPostGoToCreateForm()
        {
            return RedirectToPage("/Stock/Product");
        }
        public ActionResult OnPostEditForm(int formId, string typeform)
        {
            //วนทุกสินค้าที่เลือกมา
            foreach (var item in list)
            {
                // ถ้ามีรายการสินค้าใด ที่มีจำนวนเป็น 0 จะให้แสดงข้อความแจ้งเตือนที่หน้าจอ และให้กรอกใหม่
                if (item.qty <= 0)
                {
                    ModelState.AddModelError(string.Empty, "กรุณากรอกจำนวนสินค้าให้ครบถ้วน (มีสินค้าบางรายการที่จำนวนไม่ถูกต้อง)");
                    return OnGet(formId, typeform,null);
                }
            }

            if (typeform == typeInText)
            {
                FormIn formIn = new FormIn();

                formIn = (from t1 in db.FormIn
                          where t1.FormInid == formHeader.Formid
                          select t1).SingleOrDefault();

                if (formIn.Isstatus == false)
                {
                    foreach (var item in list)
                    {

                        var profornin = (from t1 in db.ProductFormIn
                                         where t1.ProductId == item.productid && t1.FormInid == formHeader.Formid
                                         select t1).SingleOrDefault();

                        if (item.qty != null)
                        {
                            profornin.Qty = item.qty.Value;

                            db.SaveChanges();
                        }
                    }
                }
                return RedirectToPage("./confirmstatus", "OnGet", new { formid = formHeader.Formid, formtype = "FormIn" });
            }
            else
            {

                FormOut formOut = new FormOut();

                formOut = (from t1 in db.FormOut
                           where t1.FormOutid == formHeader.Formid
                           select t1).SingleOrDefault();

                if (formOut.Isstatus == false)
                {
                    foreach (var item in list)
                    {
                        var profornout = (from t1 in db.Product
                                         where t1.Productid == item.productid
                                         select t1).SingleOrDefault();

                        if (item.qty > profornout.Qty)
                        {

                    
                            return RedirectToPage("./confirmstatus", "OnGet", new { formid = formHeader.Formid, formtype = "FormOut" ,err= profornout.Productname });
                        }
                    }

                    foreach (var item in list)
                    {
                        var profornout = (from t1 in db.ProductFormOut
                                         where t1.ProductId == item.productid && t1.FormOutid == formHeader.Formid
                                         select t1).SingleOrDefault();

                        if (item.qty != null)
                        {
                            profornout.Qty = item.qty.Value;

                            db.SaveChanges();
                        }
                    }
                }
                return RedirectToPage("./confirmstatus", "OnGet", new { formid = formHeader.Formid, formtype = "FormOut" });
            }
        }
    }
}