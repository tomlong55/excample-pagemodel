﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.ExternalModels;

namespace stockproject.Pages.Admin
{
    public class AdminModel : PageModel
    {
        /*
         * หน้า admin หน้านี้มีไว้ดูสรุปว่ามีบิลกี่บิลในระบบ ณ ปัจจุบัน
         * คำสั่งนี้มีไว้ขอใช้ Database โดยจะใช้ผ่านตัวแปรที่ชื่อว่า db
         */

        private readonly StockprojectContext db = new StockprojectContext();

        //คุณสมบัติ BindProperty ทำให้ตัวแปรที่อยู่ภายใต้อันนี้ (แค่1ตัว) สามารถรับค่าผ่านไปมาระหว่างหน้าเว็ปและหน้าที่เขียนโค้ดได้
        [BindProperty]
        public List<QueryAdmin_FormIn> listFromIn { get; set; } = new List<QueryAdmin_FormIn>();

        [BindProperty]
        public List<QueryAdmin_FormOut> listFormOut { get; set; } = new List<QueryAdmin_FormOut>();
        public List<string> id { get; set; }

        [BindProperty]
        public int totalIn { get; set; }
        [BindProperty]
        public int confirmedIn { get; set; }
        [BindProperty]
        public int rejectedIn { get; set; }
        [BindProperty]
        public int waittingIn { get; set; }

        [BindProperty]
        public int totalOut { get; set; }
        [BindProperty]
        public int confirmedOut { get; set; }
        [BindProperty]
        public int rejectedOut { get; set; }
        [BindProperty]
        public int waittingOut { get; set; }

        //ฟังชั่น onget จะเรียกทุกครั้งที่มีการเข้าถึงหน้านี้ เช่น การโหลดหน้าเว็ปหน้านี้ หรือเข้า Url นี้โดย OnGet ต้องเขียนแบบนี้เท่านั้น และตามด้วยชื่อฟังชั่น แต่ถ้าไม่มีมันจะคือ Default
        public void OnGet()
        {

            /*
             * ประกาศตัวแปร q2ให้มารับค่าจากดาต้าเบสในตาราง formin
             * var คือการไม่กำหนด Type คอมไพล์เลอร์จะหา Type ที่เหมาะสมให้ตัวแปรนั้นเอง
             * from t1 in db.FormIn คือ ตั้งตัวแปรชื่อว่า t1 เอาค่าในดาต้าเบส -> ตาราง FormIn
             * select new  เลือกใหม่ เลือกคอลัม FormInid,Status,Createdate
             * { t1.FormInid, t1.Status, t1.Createdate })*/

            var q2 = (from t1 in db.FormIn
                      select new { t1.FormInid, t1.Status, t1.Createdate });

            //วนลูป q2 โดย foreach จะวนลูปถึงตัวสุดท้ายโดนอัตโนมัติ
            foreach (var item in q2)
            {
                //สร้างตัวแปร QueryFormIn มี Type เป็นQueryAdmin_FormIn มาเพื่อกำหนดค่าลงในตัวแปรเพื่อนำไปใช้ต่อ

                QueryAdmin_FormIn QueryFormIn = new QueryAdmin_FormIn();

                QueryFormIn.id = item.FormInid; /*กำหนดค่า QueryFormIn.id = ค่าทางด้านขวาที่เอามาจากดาต้าเบส*/
                QueryFormIn.Subject = "FormIn";
                QueryFormIn.Createdate = item.Createdate;
                QueryFormIn.Status = item.Status;
                listFromIn.Add(QueryFormIn); /*เพิ่มค่าลงตัวแปร listFromIn โดยใช้คำสั่ง Add(ค่าที่ต้องการจะใส่เข้าไป)*/

                //เมื่อทำแบบนี้จนหมดตัวแปร listFromIn จะมีขนาดเท่ากับ q2 หรือคือค่าที่นำมาจากดาต้าเบส แต่ค่าใน listFromInจะเพิ่มเติมคำว่า FormIn เพื่อแยกว่านี่คือการนำเข้าหรือออก
            }

            //q3 ทำเหมือน q2 แตกต่างที่นี่คือ FormOut
            var q3 = (from t1 in db.FormOut
                      select new { t1.FormOutid, t1.Status, t1.Createdate });


            foreach (var item in q3)
            {
                QueryAdmin_FormOut QueryFormOut = new QueryAdmin_FormOut();
                QueryFormOut.id = item.FormOutid;
                QueryFormOut.Subject = "FormOut";
                QueryFormOut.Createdate = item.Createdate;
                QueryFormOut.Status = item.Status;
                listFormOut.Add(QueryFormOut);
            }
            summayReport();
        }

        /*
         * OnPost คือคำสั่งการกำหนด methord ให้ทำงานแบบ post หรือคือการส่งค่าผ่าน <from></from>
         * โดย OnPost ต้องเขียนแบบนี้เท่านั้นและตามด้วยชื่อฟังชั่น โดยทำผ่าน action ที่เรียกว่า ActionResult เท่านั้น
         */

        //ฟังชั่นนี้ไว้สำหรับการรับค่าและส่งไปให้หน้า confirmstatus โดยมี Parmmeter 2 ตัวคือ id ของ form และชนิดของ formt หรือtype
        public ActionResult OnPostVALUE(int id, string type)
        {
            /*คำสั่งส่งค่าข้ามหน้า ต้องทำผ่าน action ActionResult เท่านั้น  
             * โดย "./confirmstatus" คือการสั่งให้ไปที่หน้านี้ . (จุด) คือหมายถึงลำดับชั้นไฟล์เดียวกัน หรือคือให้ออกจากหน้านี้ไปต้องออกจากโฟล์เดอร์นี้
             * .. (จุดจุด) หมายถึงให้ออกจากโฟล์เดอรืนี้ 
             * "OnGet" คือการสั่งให้ไปที่ฟังชั่นนี้ 
             * new { formid = id, formtype = type } สั่งให้ส่ง Parmeter 2ตัวคือ id,type
             
             */
            return RedirectToPage("./confirmstatus", "OnGet", new { formid = id, formtype = type });
        }

        //ฟังชีั่น GoToCreateForm หน้าที่คือถ้ากดปุ่มนี้จะเปี่ลยหน้าไปที่ Product
        public ActionResult OnPostGoToCreateForm()
        {
            return RedirectToPage("/Stock/Product");
        }

       public void summayReport()
        {
            var inList = (from t1 in db.FormIn select t1);
            totalIn = inList.Count();
            confirmedIn = inList.Where(c => c.Status == "confirm" && c.Isstatus == true).Count();
            rejectedIn = inList.Where(c => c.Status == "reject" && c.Isstatus == true).Count();
            waittingIn = inList.Where(c => c.Status == "wait" && c.Isstatus == false).Count();
            var outList = (from t1 in db.FormOut select t1);
            totalOut = outList.Count();
            confirmedOut = outList.Where(c => c.Status == "confirm" && c.Isstatus == true).Count();
            rejectedOut = outList.Where(c => c.Status == "reject" && c.Isstatus == true).Count();
            waittingOut = outList.Where(c => c.Status == "wait" && c.Isstatus == false).Count();
        }

    }
}