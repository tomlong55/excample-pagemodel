﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using stockproject.Enititys;

namespace stockproject
{
    public class CreateUserModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();

        public void OnGet()
        {
            roledb = (from t1 in db.Role
                      select new SelectListItem
                      {
                          Value = t1.Roleid.ToString(),
                          Text = t1.Rolename
                      }).ToList();
        }
        public ActionResult OnPostToCreate()
        {
           
            User user = new User();
            user.Username = userinput.Username;
            user.Password = userinput.Password;
            user.FullName = userinput.FullName;
            user.Phone = userinput.Phone;
            user.Contact = userinput.Contact;
            user.Roleid = Convert.ToInt32(roleid);

            db.User.Add(user);
            db.SaveChanges();

            return RedirectToPage("./Admin", "OnGet");
        }



        [BindProperty]
        public string roleid { get; set; }
        [BindProperty]
        public User userinput { get; set; } = new User();
        public List<SelectListItem> roledb { get; set; } = new List<SelectListItem>();

    }
}