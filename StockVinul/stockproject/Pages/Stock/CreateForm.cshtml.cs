﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;

namespace stockproject.Pages.Stock
{
    public class CreateFormModel : PageModel
    {
        private readonly StockprojectContext db = new StockprojectContext();
        public ActionResult OnGet(List<int> proid = null)
        {
            products.Clear();
            foreach (var item in proid)
            {
                var aa = (from t1 in db.Product
                          where t1.Productid == item
                          select t1).SingleOrDefault();

                products.Add(aa);
            }

            return Page();
        }

        public ActionResult OnPostCreate()
        {
            //วนทุกสินค้าที่เลือกมา
            foreach (var item in products)
            {
                // ถ้ามีรายการสินค้าใด ที่มีจำนวนเป็น 0 จะให้แสดงข้อความแจ้งเตือนที่หน้าจอ และให้กรอกใหม่
                if (item.Qty <= 0)
                {
                    ModelState.AddModelError(string.Empty, "กรุณากรอกจำนวนสินค้าให้ครบถ้วน (มีสินค้าบางรายการที่จำนวนไม่ถูกต้อง)");
                    proid = products.Select(c => c.Productid).ToList();
                    OnGet(proid);
                    return Page();
                }
            }
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());

            var aa = products;
            var a = formId;

            if (a == "1") // in
            {
                int id = 0;
                FormIn formIn = new FormIn();
                //var a = User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault())
                formIn.Userid = userid; //this.User.Identity.Name
                //formIn.Status = false;
                formIn.Createdate = DateTime.Now;

                db.FormIn.Add(formIn);

                db.SaveChanges();
                id = formIn.FormInid;

                foreach (var item in products)
                {
                    ProductFormIn productFormIns = new ProductFormIn();

                    productFormIns.FormInid = id;
                    productFormIns.ProductId = item.Productid;
                    productFormIns.Qty = item.Qty;
                    productFormIns.Price = item.Price;

                    db.ProductFormIn.Add(productFormIns);
                }
           
                db.SaveChanges();
            }
            else if (a == "2") // out
            {
                foreach (var item in products)
                {
                    var checkstock = (from t1 in db.Product
                                      where t1.Productid == item.Productid
                                      select t1).SingleOrDefault();
                    if (checkstock.Qty < item.Qty)
                    {

                        ModelState.AddModelError(string.Empty, "'"+checkstock.Productname+"' ของใน Stock มีไม่พอ");
                        proid = products.Select(c => c.Productid).ToList();
                        OnGet(proid);
                        return Page();
                        //return RedirectToPage("./Product"); // ของมีไม่ถึง
                    }
                }

                int id = 0;
                FormOut formOut = new FormOut();

                formOut.Userid = userid;
                //formIn.Status = false;
                formOut.Createdate = DateTime.Now;

                db.FormOut.Add(formOut);
                db.SaveChanges();
                id = formOut.FormOutid;

                foreach (var item in products)
                {
                    ProductFormOut productFormOut = new ProductFormOut();

                    productFormOut.FormOutid = id;
                    productFormOut.ProductId = item.Productid;
                    productFormOut.Qty = item.Qty;
                    productFormOut.Price = item.Price;

                    db.ProductFormOut.Add(productFormOut);
                }
                db.SaveChanges();
            }
            return RedirectToPage("./Product");
        }

        [BindProperty]
        public List<int> proid { get; set; }

        [BindProperty]
        public string formId { get; set; }

        [BindProperty]
        public List<Product> products { get; set; } = new List<Product>();
    }
}