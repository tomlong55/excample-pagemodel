﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;

namespace stockproject.Pages.Stock
{
    public class ProductModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();
        public List<Product> list { get; set; } = new List<Product>();
        [BindProperty]
        public List<int> prid { get; set; }
        [BindProperty]
        public bool? viewMode { get; set; } = true;

        public ActionResult OnGet(bool mode)
        {
            viewMode = mode;
            list = (from t1 in db.Product
                    select t1).ToList();

            return Page();
        }
        public ActionResult OnPostToCreate()
        {
            if (prid != null || prid.Count != 0)
            {
                return RedirectToPage("./CreateForm", "OnGet", new { proid = prid });
            }
            else
            {
                return Page();
            }
        }
        public ActionResult OnPostToCreateProduct()
        {
            return RedirectToPage("/Stock/CreateProduct");
        }
        public ActionResult OnPostToUpdateProduct(List<Product> list)
        {
            foreach (var item in list)
            {
                //Product product = new Product();
                var listp = (from t1 in db.Product
                             where t1.Productid == item.Productid
                             select t1).SingleOrDefault();


                listp.Productname = item.Productname;
                listp.Description = item.Description;
                listp.Discount = item.Discount;
                listp.Price = item.Price;
                listp.Modifydate = DateTime.Now;
            }
            db.SaveChanges();

            //return OnGet(false);
            return RedirectToPage("./Product", "OnGet", new { mode = false });
        }

        public ActionResult OnPostChangeView()
        {
            //var a = viewMode.Value;
            //viewMode = null;
            //ModelState.Clear();

            //viewMode = a;
            return RedirectToPage("/Stock/Product", "OnGet", new { mode = !viewMode });
        }
    }
}