﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.ExternalModels;

namespace stockproject
{
    public class FormDetailModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();
        public ActionResult OnGet(int formId, string formtype)
        {
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
            var Username = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ViewData["name"] = Username;
            if (formtype == "in")
            {
                ViewData["id"] = formId;
                ViewData["type"] = "รายการนำสินค้าเข้า";
                formHeader = db.FormIn.Select(a => new ResultForm()
                {
                    Formid = a.FormInid,
                    IsIn = true,
                    IsStatus = a.Isstatus,
                    Status = a.Status,
                    Createdate = a.Createdate,
                    Statusdate = a.Statusdate,
                    Userid = a.Userid

                }).Single(a => a.Formid == formId && a.Userid == userid);
                if (formHeader.Userid == userid)
                {
                    ViewData["Ckuser"] = "Yes";
                    ViewData["typeForm"] = "Formin";
                    ViewData["IDForm"] = formId;
                }
                else
                {
                    ViewData["Ckuser"] = "No";
                }
                formDetail = db.ProductFormIn.Where(c => c.FormInid == formId).Select(a => new DetailForm()
                {
                    ProductformId = a.ProductformInid,
                    ProductName = db.Product.Where(pro => pro.Productid == a.ProductId).Single().Productname,
                    FormId = a.FormInid,
                    Price = a.Price,
                    ProductId = a.ProductId,
                    Qty = a.Qty,
                    Status = a.Status,
                    Picpath = db.Product.Where(pro => pro.Productid == a.ProductId).Single().Picpath
                }
                ).ToList();
            }
            else
            {
                ViewData["id"] = formId;
                ViewData["type"] = "รายการจ่ายสินค้าออก";
                formHeader = db.FormOut.Select(a => new ResultForm()
                {
                    Formid = a.FormOutid,
                    IsIn = false,
                    IsStatus = a.Isstatus,
                    Status = a.Status,
                    Createdate = a.Createdate,
                    Statusdate = a.Statusdate,
                    Userid = a.Userid
                }).Single(a => a.Formid == formId && a.Userid == userid);
                if (formHeader.Userid == userid)
                {
                    ViewData["Ckuser"] = "Yes";
                    ViewData["typeForm"] = "Formout";
                    ViewData["IDForm"] = formId;
                }
                else
                {
                    ViewData["Ckuser"] = "No";
                }
                formDetail = db.ProductFormOut.Where(c => c.FormOutid == formId).Select(a => new DetailForm()
                {
                    ProductformId = a.ProductformOutid,
                    ProductName = db.Product.Where(pro => pro.Productid == a.ProductId).Single().Productname,
                    FormId = a.FormOutid,
                    Price = a.Price,
                    ProductId = a.ProductId,
                    Qty = a.Qty,
                    Status = a.Status,
                    Picpath = db.Product.Where(pro => pro.Productid == a.ProductId).Single().Picpath
                }
                ).ToList();
            }
            ViewData["date"] = formHeader.Createdate;
            if (formHeader.IsStatus == true)
            {
                ViewData["status"] = "True";
            }
            else
            {
                ViewData["status"] = "False";
            }
            return Page();
        }
        public ActionResult OnPostEditForm(string typeform)
        {
            //วนทุกสินค้าที่เลือกมา
            foreach (var item in formDetail)
            {
                // ถ้ามีรายการสินค้าใด ที่มีจำนวนเป็น 0 จะให้แสดงข้อความแจ้งเตือนที่หน้าจอ และให้กรอกใหม่
                if (item.Qty <= 0)
                {
                    ModelState.AddModelError(string.Empty, "กรุณากรอกจำนวนสินค้าให้ครบถ้วน (มีสินค้าบางรายการที่จำนวนไม่ถูกต้อง)");
                    return OnGet(formHeader.Formid, (typeform == "Formin") ? "in" : "out");
                }
            }
            string formType;

            if (typeform == "Formin")
            {
                formType = "in";
                FormIn formIn = new FormIn();

                formIn = (from t1 in db.FormIn
                          where t1.FormInid == formHeader.Formid
                          select t1).SingleOrDefault();

                if (formIn.Isstatus == false)
                {
                    foreach (var item in formDetail)
                    {

                        var profornin = (from t1 in db.ProductFormIn
                                         where t1.ProductId == item.ProductId && t1.FormInid == item.FormId
                                         select t1).SingleOrDefault();

                        if (item.Qty != null)
                        {
                            profornin.Qty = item.Qty.Value;

                            db.SaveChanges();
                        }
                    }
                }
            }
            else
            {
                formType = "out";
                FormOut formOut = new FormOut();

                formOut = (from t1 in db.FormOut
                           where t1.FormOutid == formHeader.Formid
                           select t1).SingleOrDefault();

                if (formOut.Isstatus == false)
                {
                    foreach (var item in formDetail)
                    {
                        var profornin = (from t1 in db.Product
                                         where t1.Productid == item.ProductId
                                         select t1).SingleOrDefault();

                        if (item.Qty > profornin.Qty)
                        {
                            ModelState.AddModelError(string.Empty, "'" + item.ProductName + "' ของใน Stock มีไม่พอ");
                            return OnGet(formHeader.Formid, formType);
                        }
                    }

                    foreach (var item in formDetail)
                    {
                        var profornin = (from t1 in db.ProductFormOut
                                         where t1.ProductId == item.ProductId && t1.FormOutid == item.FormId
                                         select t1).SingleOrDefault();

                        if (item.Qty != null)
                        {
                            profornin.Qty = item.Qty.Value;

                            db.SaveChanges();
                        }
                    }
                }
            }
            //foreach (var item in collection)
            //{

            //}
            //var a = formDetail;
            //var b = formHeader;
            //ModelState.Clear();
            return OnGet(formHeader.Formid, formType);
        }

        public ActionResult OnPostGoToCreateForm()
        {
            return RedirectToPage("/Stock/Product");
        }
        public ActionResult OnPostDeleteForm(int formId, string typeform)
        {
            if (typeform == "Formin")
            {

                var query = from c in db.FormIn where (c.FormInid == formId) select c;
                foreach (FormIn c in query)
                {
                    db.FormIn.Remove(c);

                }

                var query2 = from c2 in db.ProductFormIn where (c2.FormInid == formId) select c2;
                foreach (ProductFormIn c2 in query2)
                {
                    db.ProductFormIn.Remove(c2);

                }
                db.SaveChanges();

            }
            else
            {
                var query = from c in db.FormOut where (c.FormOutid == formId) select c;
                foreach (FormOut c in query)
                {
                    db.FormOut.Remove(c);

                }

                var query2 = from c2 in db.ProductFormOut where (c2.FormOutid == formId) select c2;
                foreach (ProductFormOut c2 in query2)
                {
                    db.ProductFormOut.Remove(c2);

                }
                db.SaveChanges();
            }
            return RedirectToPage("/Stock/User/User");
        }

        [BindProperty]
        public ResultForm formHeader { get; set; }
        [BindProperty]
        public List<DetailForm> formDetail { get; set; } = new List<DetailForm>();
    }
}