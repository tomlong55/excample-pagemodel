﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.ExternalModels;

namespace stockproject
{
    //[Authorize]
    public class UserModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();
        public void OnGet()
        {
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());

            Formin = (from t1 in db.FormIn
                      where t1.Userid == userid
                      select t1).ToList();

            Formout =  (from t1 in db.FormOut
                       select t1).Where(c => c.Userid == userid).ToList();
                
            //List<ResultForm> formInList = db.FormIn.Select(a => new ResultForm()
            //{
            //    Formid = a.FormInid,
            //    IsIn = true,
            //    Status = a.Status,
            //    Createdate = a.Createdate,
            //    Statusdate = a.Statusdate,
            //    Userid = a.Userid
            //}).ToList();
            //List<ResultForm> formOutList = db.FormOut.Select(a => new ResultForm()
            //{
            //    Formid = a.FormOutid,
            //    IsIn = false,
            //    Status = a.Status,
            //    Createdate = a.Createdate,
            //    Statusdate = a.Statusdate,
            //    Userid = a.Userid
            //}).ToList();
            //ResultList.AddRange(formInList);
            //ResultList.AddRange(formOutList);
        }
        public ActionResult OnPostVALUE(int id, string type)
        {
            /*คำสั่งส่งค่าข้ามหน้า ต้องทำผ่าน action ActionResult เท่านั้น  
             * โดย "./FormDetail" คือการสั่งให้ไปที่หน้านี้ . (จุด) คือหมายถึงลำดับชั้นไฟล์เดียวกัน หรือคือให้ออกจากหน้านี้ไปต้องออกจากโฟล์เดอร์นี้
             * .. (จุดจุด) หมายถึงให้ออกจากโฟล์เดอรืนี้ 
             * "OnGet" คือการสั่งให้ไปที่ฟังชั่นนี้ 
             * new { formid = id, formtype = type } สั่งให้ส่ง Parmeter 2ตัวคือ id,type
             
             */
            return RedirectToPage("./FormDetail", "OnGet", new { formid = id, formtype = type });
        }
        public ActionResult OnPostGoToCreateForm()
        {
            return RedirectToPage("/Stock/Product");
        }

        [BindProperty]
        public List<ResultForm> ResultList { get; set; } = new List<ResultForm>();
        public List<FormIn> Formin { get; set; } = new List<FormIn>();
        public List<FormOut> Formout { get; set; } = new List<FormOut>();
        [BindProperty]
        public ResultForm SelectedForm { get; set; }
    }
}