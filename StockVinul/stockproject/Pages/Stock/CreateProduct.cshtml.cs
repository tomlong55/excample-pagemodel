﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;

namespace stockproject
{
    public class CreateProductModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();

        private IHostingEnvironment _environment;
        public CreateProductModel(IHostingEnvironment environment)
        {
            _environment = environment;
        }
        public ActionResult OnGet(string _name, string _description, string _price, float _discount)
        {
            name = _name;
            description = _description;
            price = _price;
            discount = _discount;
            return Page();
        }
        public ActionResult OnPostToCreateAsync()
        {
            var userid = Convert.ToInt32(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault());
            if (name == null || price == null)
            {
                return Page();
            }

            var a = name;
            var a1 = price;
            // ถ้า ไม่มีการอัพโหลดภาพเข้าระบบ จะให้แสดงข้อความแจ้งเตือนที่หน้าจอ และให้ทำรายการใหม่
            if (Upload == null)
            {
                //ModelState.AddModelError(string.Empty, "กรุณาเพิ่มรูปภาพสินค้า");
                OnGet(name, description, price, discount);
                return Page();
            }
            string newfileName = DateTime.Now.Ticks.ToString() + "_" + Upload.FileName;
            var path = Path.Combine(_environment.WebRootPath, "Pic", newfileName);

            Product table = new Product();
            table.Productname = name;
            table.Qty = 0;
            table.Price = Int32.Parse(price);
            table.Userid = userid;
            table.Picpath = newfileName;
            table.Modifydate = DateTime.Now;
            table.Description = description;
            table.Discount = discount;

            FileStream stream = new FileStream(path, FileMode.Create);
            Upload.CopyToAsync(stream);

            db.Product.Add(table);
            db.SaveChanges();

            return RedirectToPage("./Product", "OnGet");
        }



        //public void OnPostTest()
        //{

        //}
        [Required(ErrorMessage = "กรุณาเพิ่มรูปภาพสินค้า")]
        [BindProperty]
        public IFormFile Upload { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "กรุณาใส่ชื่อสินค้า")]
        public string name { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "กรุณาใส่ราคาสินค้า")]
        public string price { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "กรุณาใส่คำอธิบายสินค้า")]
        public string description { get; set; }
        [BindProperty]
        [Required(ErrorMessage = "กรุณาใส่ส่วนลด หากไม่มีกรุณากรอก 0")]
        public float discount { get; set; }
    }
}