﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace stockproject.ExternalModels
{
    public class ResultForm
    {
        public int Formid { get; set; }
        public bool IsIn { get; set; }
        public bool IsStatus { get; set; }
        public int? Userid { get; set; }
        public string Status { get; set; }
        public DateTime? Statusdate { get; set; }
        public DateTime? Createdate { get; set; }
    }
}
