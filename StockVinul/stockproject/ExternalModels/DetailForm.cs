﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace stockproject.ExternalModels
{
    public class DetailForm
    {
        public int ProductformId { get; set; }
        public string ProductName { get; set; }
        public int? FormId { get; set; }
        public int? ProductId { get; set; }
        public int? Qty { get; set; }
        public float? Price { get; set; }
        public bool? Status { get; set; }
        public string Picpath { get; set; }
    }
}
