﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace stockproject.ExternalModels
{
    public class QueryAdmin_FormIn
    {
        public int id { get; set; }
        public string Subject { get; set; }
        public DateTime? Createdate { get; set; }
        public string Status { get; set; }


    }
    public class QueryAdmin_FormOut
    {
        public int id { get; set; }
        public string Subject { get; set; }
        public DateTime? Createdate { get; set; }
        public string Status { get; set; }


    }
}
